<UserControl
    x:Class="TestAdministration.Views.Testing.Instructions.Ppt.PptInstructionsDominantHandFirst"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:ui="http://schemas.lepo.co/wpfui/2022/xaml"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:viewModelInstructionsPpt="clr-namespace:TestAdministration.ViewModels.Testing.Instructions.Ppt"
    mc:Ignorable="d"
    d:DataContext="{d:DesignInstance viewModelInstructionsPpt:PptInstructionsDominantHandFirstViewModel}">

    <UserControl.Resources>
        <ResourceDictionary Source="/Views/Testing/Instructions/InstructionsDictionary.xaml" />
    </UserControl.Resources>

    <StackPanel Margin="{StaticResource ContentMargin}">
        <Grid Margin="0,0,0,16">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="Auto" />
            </Grid.RowDefinitions>

            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="0.1*" />
                <ColumnDefinition Width="0.3*" />
                <ColumnDefinition Width="0.1*" />
                <ColumnDefinition Width="0.65*" />
                <ColumnDefinition Width="0.1*" />
            </Grid.ColumnDefinitions>

            <ui:TextBlock
                Grid.Row="0"
                Grid.Column="1"
                Margin="0,0,0,8"
                HorizontalAlignment="Center"
                VerticalAlignment="Center"
                TextWrapping="Wrap"
                FontTypography="Subtitle"
                Foreground="{DynamicResource TextControlForeground}"
                Text="Výchozí pozice" />

            <Image
                Grid.Row="1"
                Grid.Column="1"
                x:Name="InitPositionImage"
                Source="/Resources/Images/PptInitial.jpg" />

            <ui:TextBlock
                Grid.Row="0"
                Grid.Column="3"
                Margin="0,0,0,8"
                HorizontalAlignment="Center"
                VerticalAlignment="Center"
                TextWrapping="Wrap"
                FontTypography="Subtitle"
                Foreground="{DynamicResource TextControlForeground}">

                Rozmístění součástek pro
                <Run Text="{Binding DominantHandPlural,Mode=OneWay}" />
            </ui:TextBlock>

            <Image
                Grid.Row="1"
                Grid.Column="3"
                Height="{Binding ActualHeight,ElementName=InitPositionImage}"
                Source="{Binding ImagePath}" />
        </Grid>

        <ui:TextBlock>
            Vyzvěte probanda, ať se posadí ke stolu na pevnou židli. Sami se posaďte naproti němu. Výšku stolu nastavte
            tak, aby na něm mohl mít položenou alespoň polovinu předloktí za vzpřímeného sedu. Desku umístěte na stůl
            tak, aby
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">
                řada čtyř zásobníků byla v horní části desky
            </Span>
            (odsuňte kryt s názvem testu).
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">
                Zarovnejte spodní hranu desky s okrajem stolu a střed desky přibližně se středem trupu probanda
            </Span>
            . Každý ze zásobníků úplně vpravo a úplně vlevo by měl obsahovat 25 kolíků (celkový počet 50 kolíků).
            Pro probandy s dominantní
            <Run Text="{Binding DominantHandInstrumental, Mode=OneWay}" />
            rukou bude v zásobníku
            <Run Text="{Binding WashersSide, Mode=OneWay}" />
            od středu umístěno 40 podložek a
            <Run Text="{Binding CollarsSide,Mode=OneWay}" />
            od středu 40 trubiček.
            <LineBreak />
            Když je proband usazen a připraven začít, řekněte / přehrajte nahrávku:
            <LineBreak />
            <Span Style="{StaticResource AudioInstructionSpanStyle}">
                „Tento test má ukázat, jak rychle a přesně umíte pracovat rukama. Před každým dílčím testem vám řeknu,
                co máte dělat, a pak budete mít příležitost si to vyzkoušet. Je důležité, abyste přesně porozuměl/a
                tomu, co máte dělat.“
            </Span>
        </ui:TextBlock>

        <ContentPresenter
            Margin="{StaticResource AudioPlayerMargin}"
            Content="{Binding FirstAudioInstructionViewModel}" />

        <ui:TextBlock>
            <Span FontWeight="Bold" TextDecorations="Underline">
                Poznámka pro předvádění ukázek ve všech subtestech:
            </Span>
            <LineBreak />
            Ukázky provedení jednotlivých subtestů vždy ukazujte z místa, kde sedíte. Vše tedy budete provádět opačnou
            rukou než proband (zrcadlově).
            <LineBreak />
            Začněte tím, že řeknete / přehrajete nahrávku a
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">zároveň ukážete</Span>:
            <LineBreak />
            <Span Style="{StaticResource AudioInstructionSpanStyle}">
                <Run Text="{Binding PracticeAudioInstruction,Mode=OneWay}" />
            </Span>
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">
                (Umístěte hned do dalšího otvoru ještě jeden kolík. Oba je pak nechte v otvorech.)
            </Span>
            <LineBreak />
            <Span Style="{StaticResource AudioInstructionSpanStyle}">
                „Pokud v průběhu testování kolík upustíte, nezastavujte a nesbírejte jej. Jednoduše pokračujte tím, že
                si ze zásobníku vezmete další kolík. Položte obě ruce po stranách desky. Nyní si vyzkoušejte umístit
                alespoň tři kolíky.“
            </Span>
        </ui:TextBlock>

        <ContentPresenter
            Margin="{StaticResource AudioPlayerMargin}"
            Content="{Binding SecondAudioInstructionViewModel}" />

        <ui:TextBlock>
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">Opravte veškeré chyby</Span>
            provedené při umisťování kolíků a
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">zodpovězte</Span>
            všechny dotazy.
            <LineBreak />
            Poté, co proband
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">umístil tři nebo čtyři kolíky</Span>
            a je zřejmé, že zadání porozuměl, zapište si případné poznámky a řekněte / přehrajte nahrávku:
            <LineBreak />
            <Span Style="{StaticResource AudioInstructionSpanStyle}">
                „Stačí. Nyní vyndejte zkušební kolíky a vraťte je zpět do zásobníku
                <Run Text="{Binding PinsSide,Mode=OneWay}" />.“
            </Span>
        </ui:TextBlock>

        <ContentPresenter
            Margin="{StaticResource AudioPlayerMargin}"
            Content="{Binding ThirdAudioInstructionViewModel}" />

        <ui:TextBlock>
            Poté, co proband dokončí tento úkol, řekněte / přehrajte nahrávku:
            <LineBreak />
            <Span Style="{StaticResource AudioInstructionSpanStyle}">
                <Run Text="{Binding TrialAudioInstruction,Mode=OneWay}" />
            </Span>
        </ui:TextBlock>

        <ContentPresenter
            Margin="{StaticResource AudioPlayerMargin}"
            Content="{Binding FourthAudioInstructionViewModel}" />

        <ui:TextBlock>
            Řekněte:
            <Span Style="{StaticResource OralInstructionSpanStyle}">„Teď!“</Span>.
            Současně s vydáním pokynu „Teď!“
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">spusťte odpočítávání 30 sekund</Span>.
            <LineBreak />
            Po uplynutí přesně 30 sekund, řekněte:
            <Span Style="{StaticResource OralInstructionSpanStyle}">„Stop!“</Span>.
            <LineBreak />
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">Spočítejte</Span>
            množství správně umístěných
            <Span Style="{StaticResource PhysicalInstructionSpanStyle}">kolíků</Span>
            pravou rukou do otvorů a výsledek zaznamenejte. Využívejte prostor pro psaní poznámek z pozorování výkonu
            probanda. Stejným způsobem postupujte i u dalších pokusů tohoto subtestu.
            <LineBreak />
            Pak vyzvěte probanda / přehrajte nahrávku:
            <LineBreak />
            <Span Style="{StaticResource AudioInstructionSpanStyle}">
                „Děkuji. Nyní, prosím, vraťte kolíky zpět do zásobníku
                <Run Text="{Binding PinsSide,Mode=OneWay}" />.“
            </Span>
        </ui:TextBlock>

        <ContentPresenter
            Margin="{StaticResource AudioPlayerMargin}"
            Content="{Binding FifthAudioInstructionViewModel}" />
    </StackPanel>
</UserControl>