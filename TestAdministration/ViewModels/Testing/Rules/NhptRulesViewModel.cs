namespace TestAdministration.ViewModels.Testing.Rules;

/// <summary>
/// A view model for binding a view containing list or rules
/// for conducting Nine Hole Peg Test.
/// </summary>
public class NhptRulesViewModel : ViewModelBase;