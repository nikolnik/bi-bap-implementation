namespace TestAdministration.ViewModels.Testing.Rules;

/// <summary>
/// A view model for binding a view containing list or rules
/// for conducting Purdue Pegboard Test.
/// </summary>
public class PptRulesViewModel : ViewModelBase;