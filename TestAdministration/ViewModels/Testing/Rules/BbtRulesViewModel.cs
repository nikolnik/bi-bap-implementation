namespace TestAdministration.ViewModels.Testing.Rules;

/// <summary>
/// A view model for binding a view containing list or rules
/// for conducting Box and Block Test.
/// </summary>
public class BbtRulesViewModel : ViewModelBase;