namespace TestAdministration.ViewModels;

/// <summary>
/// A view model for binding initial content of the main screen.
/// </summary>
public class InitContentViewModel : ViewModelBase;