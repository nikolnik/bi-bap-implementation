namespace TestAdministration.Models.Data;

/// <summary>
/// An enum of available test types.
/// </summary>
public enum TestType
{
    /// <summary>
    /// Nine Hole Peg Test.
    /// </summary>
    Nhpt,

    /// <summary>
    /// Purdue Pegboard Test.
    /// </summary>
    Ppt,

    /// <summary>
    /// Box and Block Test.
    /// </summary>
    Bbt
}