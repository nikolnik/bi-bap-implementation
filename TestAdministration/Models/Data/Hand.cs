namespace TestAdministration.Models.Data;

/// <summary>
/// An enum for specifying patients' dominant or pathological hands.
/// </summary>
public enum Hand
{
    None,
    Left,
    Right,
    Both
}