namespace TestAdministration.Models.Storages.Records;

/// <summary>
/// A static class for definition of common CSV value formats.
/// </summary>
public static class CsvRecordConfiguration
{
    public const string Delimiter = ";";
    public const string Culture = "cs";
    public const string DateFormat = "dd.MM.yyyy";
    public const string TimeFormat = "HH:mm";
    public const string FloatFormat = "0.##";
}