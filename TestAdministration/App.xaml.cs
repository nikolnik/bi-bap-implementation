﻿using System.Windows;
using System.Windows.Threading;
using Microsoft.Extensions.DependencyInjection;
using TestAdministration.Models.Services;
using TestAdministration.Models.Storages;
using TestAdministration.Models.Storages.Converters;
using TestAdministration.Models.Storages.Exporters;
using TestAdministration.Models.Storages.FileSystems;
using TestAdministration.Models.Storages.Importers;
using TestAdministration.Models.TestBuilders;
using TestAdministration.Models.TestBuilders.SectionBuilders;
using TestAdministration.Models.TestBuilders.SectionBuilders.Calculators;
using TestAdministration.Models.Utils;
using TestAdministration.ViewModels;
using TestAdministration.ViewModels.Testing;
using TestAdministration.ViewModels.Testing.Results;
using TestAdministration.Views;
using Wpf.Ui;
using Wpf.Ui.Appearance;
using MessageBox = Wpf.Ui.Controls.MessageBox;

namespace TestAdministration;

public partial class App
{
    protected override void OnStartup(StartupEventArgs e)
    {
        base.OnStartup(e);

        DispatcherUnhandledException += _onDispatcherUnhandledException;

        var serviceProvider = _configureServices().BuildServiceProvider();

        var configurationService = serviceProvider.GetService<ConfigurationService>();
        if (configurationService is null)
        {
            throw new InvalidOperationException($"Missing {typeof(ConfigurationService)} service");
        }

        ApplicationThemeManager.Apply(configurationService.ApplicationTheme);

        var fontSize = configurationService.FontSize;
        Current.Resources["BaseFontSize"] = (double)fontSize;
        Current.Resources["ControlContentThemeFontSize"] = (double)fontSize;

        var mainWindowViewModel = serviceProvider.GetService<MainWindowViewModel>();
        if (mainWindowViewModel is null)
        {
            throw new InvalidOperationException($"Missing {typeof(MainWindowViewModel)} service");
        }

        var mainWindow = new MainWindow(mainWindowViewModel);

        MainWindow = mainWindow;
        MainWindow.Show();
        MainWindow.WindowState = WindowState.Maximized;
    }

    private static IServiceCollection _configureServices() =>
        new ServiceCollection()
            .AddSingleton<MainWindowViewModel>()
            .AddSingleton<LoginScreenViewModel>()
            .AddSingleton<ConfigurationService>()
            .AddSingleton<IContentDialogService, ContentDialogService>()
            .AddSingleton<MainScreenViewModel>()
            .AddSingleton<SettingsViewModelFactory>()
            .AddSingleton<ITestStorage, TestStorage>()
            .AddSingleton<LocalFileSystem>()
            .AddSingleton<IFileSystem>(s =>
                s.GetService<LocalFileSystem>()
                ?? throw new InvalidOperationException($"Missing {typeof(LocalFileSystem)} service")
            )
            .AddSingleton<ICsvImporter, LocalCsvImporter>()
            .AddSingleton<ICsvExporter, LocalCsvExporter>()
            .AddSingleton<PatientCsvConverter>()
            .AddSingleton<TestingViewModelFactory>()
            .AddSingleton<TestConductionViewModelFactory>()
            .AddSingleton<ResultsViewModelFactory>()
            .AddSingleton<ITestBuilderFactory, TestBuilderFactory>()
            .AddSingleton<NhptTestSectionBuilder>()
            .AddSingleton<PptTestSectionBuilder>()
            .AddSingleton<BbtTestSectionBuilder>()
            .AddSingleton<ITestCalculator<NhptTestNormProvider>, TestCalculator<NhptTestNormProvider>>()
            .AddSingleton<ITestCalculator<PptTestNormProvider>, TestCalculator<PptTestNormProvider>>()
            .AddSingleton<ITestCalculator<BbtTestNormProvider>, TestCalculator<BbtTestNormProvider>>()
            .AddSingleton<IAgeCalculatorService, AgeCalculatorService>()
            .AddSingleton<IDateTimeProvider, DateTimeProvider>()
            .AddSingleton<NhptTestNormProvider>()
            .AddSingleton<PptTestNormProvider>()
            .AddSingleton<BbtTestNormProvider>()
            .AddSingleton<NhptCsvConverter>()
            .AddSingleton<PptCsvConverter>()
            .AddSingleton<BbtCsvConverter>()
            .AddSingleton<VideoExporter>()
            .AddSingleton<DocumentationExporter>()
            .AddSingleton<DocumentationConverter>()
            .AddSingleton<NormInterpretationConverter>()
            .AddSingleton<AudioInstructionService>()
            .AddSingleton<VideoRecorderService>();

    private async void _onDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
    {
        var messageBox = new MessageBox
        {
            Title = "Chyba",
            Content = e.Exception.Message
        };
        _ = await messageBox.ShowDialogAsync();
    }
}